﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Budgetco.Data
{
    public interface IBudgetRepository
    {
        bool AddRecord(Record record, ApplicationUser user);
        bool RemoveRecord(Guid recordId);
        List<Record> GetRecords(ApplicationUser user);
        Record GetRecord(Guid id);
        Record EditRecord(Guid id, string name, int typeNumber, decimal amount, string description);
        decimal GetExpenseSum(ApplicationUser user);
        decimal GetIncomeSum(ApplicationUser user);
        int GetIncomesCount(ApplicationUser user);
        int GetRecordsCount(ApplicationUser user);
        int GetExpensesCount(ApplicationUser user);
        List<UserGroup> GetGroups(ApplicationUser user);
        string AddGroup(string name, List<string> emails, ApplicationUser user);
        string AddUserToGroup(string name, string email, ApplicationUser user);
        Groups GetGroup(string groupName);
        bool AddGroupRecord(Record record, Groups group);
        Record EditRecordGroup(Guid id, string name, int typeNumber, decimal amount, string description, Groups group);
    }
}
