﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Budgetco.Data
{
    public class Groups
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public List<UserGroup> Users { get; set; }
        public List<Record> Records { get; set; }
        public Groups()
        {
            Users = new List<UserGroup>();
            Records = new List<Record>();
        }

        public Groups(string name, ApplicationUser user)
        {
            Name = name;
            Id = Guid.NewGuid();
            Records = new List<Record>();
            Users = new List<UserGroup> {new UserGroup(user, this)};
        }

        public UserGroup GetUserGroups(string id)
        {
            foreach (var appUserGroup in Users)
            {
                if (appUserGroup.UserId.Equals(id)) return appUserGroup;
            }
            return null;
        }

        public void AddUsers(ICollection<ApplicationUser> users)
        {
            foreach (var user in users)
            {
                Users.Add(new UserGroup(user, this));
            }
        }
    }
}
