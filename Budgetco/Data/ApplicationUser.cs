using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Server.Kestrel.Internal.System.Collections.Sequences;

namespace Budgetco.Data
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        
        public List<Record> Records { get; set; }
        public List<UserGroup> Groups { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public ApplicationUser()
        {
            Records = new List<Record>();
            Groups = new List<UserGroup>();
        }
    }
}
