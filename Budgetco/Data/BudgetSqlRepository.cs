﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Budgetco.Constants;
using Budgetco.Services;
using Microsoft.EntityFrameworkCore;
using Remotion.Linq.Clauses;

namespace Budgetco.Data
{
    public class BudgetSqlRepository : IBudgetRepository
    {
        private readonly ApplicationDbContext _context;
        public BudgetSqlRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public bool AddRecord(Record record, ApplicationUser user)
        {
            try
            {
                _context.Records.Add(record);
                user.Records.Add(record);
                _context.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
        public bool RemoveRecord(Guid recordId)
        {
            try
            {
                _context.Entry(_context.Records.Find(recordId)).State = EntityState.Deleted;
                _context.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
        public List<Record> GetRecords(ApplicationUser user)
        {
            return _context.Users.Include(t => t.Records).First(t => t.Id.Equals(user.Id)).Records;
        }

        public Record GetRecord(Guid id)
        {
            return _context.Records.Find(id);
        }

        public Record EditRecord(Guid id, string name, int typeNumber, decimal amount, string description)
        {
            var record = GetRecord(id);
            if (record == null) return null;
            record.Amount = amount;
            record.Type = typeNumber;
            record.Description = description;
            record.Name = name;
            record.Date = DateTime.UtcNow;
            _context.SaveChanges();
            return record;
        }
        
        public decimal GetExpenseSum(ApplicationUser user)
        {
            var usr = _context.Users.Include(t => t.Records).First(t => t.Id.Equals(user.Id));
            if (usr != null)
            {
                return usr.Records.Where(t=>t.Amount<0.0m).Sum(t => t.Amount);
            }
            return -1;
        }

        public decimal GetIncomeSum(ApplicationUser user)
        {
            var usr = _context.Users.Include(t => t.Records).First(t => t.Id.Equals(user.Id));
            if (usr != null)
            {
                return usr.Records.Where(t => t.Amount >= 0.0m).Sum(t => t.Amount);
            }
            return -1;
        }

        public int GetIncomesCount(ApplicationUser user)
        {
            var usr = _context.Users.Include(t => t.Records).First(t => t.Id.Equals(user.Id));
            if (usr != null)
            {
                return usr.Records.Count(t => t.Amount >= 0.0m);
            }
            return -1;
        }

        public int GetRecordsCount(ApplicationUser user)
        {
            var usr = _context.Users.Include(t => t.Records).First(t => t.Id.Equals(user.Id));
            if (usr != null)
            {
                return usr.Records.Count();
            }
            return -1;
        }

        public int GetExpensesCount(ApplicationUser user)
        {
            var usr = _context.Users.Include(t => t.Records).First(t => t.Id.Equals(user.Id));
            if (usr != null)
            {
                return usr.Records.Count(t => t.Amount < 0.0m);
            }
            return -1;
        }
       
        public List<UserGroup> GetGroups(ApplicationUser user)
        {
            var usr = _context.Users.Include(t => t.Groups).ThenInclude(t => t.GroupNavigation).ThenInclude(t => t.Records).First(t => t.Id.Equals(user.Id));
            return usr?.Groups.Where(t => t.UserId.Equals(usr.Id)).ToList();
        }

        public string AddGroup(string name, List<string> emails, ApplicationUser user)
        {
            
            var usr = _context.Users.Include(t => t.Groups).ThenInclude(t => t.GroupNavigation).First(t => t.Id.Equals(user.Id));
            if (usr != null)
            {
                try
                {
                    Groups newGroup = new Groups(name, user);
                    List<ApplicationUser> users = _context.Users.Include(t=>t.Groups).Where(t => emails.Contains(t.Email)).ToList();
                    newGroup.AddUsers(users);
                    _context.Groups.Add(newGroup);
                    _context.SaveChanges();
                }
                catch (Exception)
                {
                    return "Group with same name already exists...\nRename your group and try again";
                }
            }
            return "Group added!";
        }

        public string AddUserToGroup(string name, string email, ApplicationUser user)
        {

            var usr = _context.Users.Include(t => t.Groups).ThenInclude(t => t.GroupNavigation).First(t => t.Id.Equals(user.Id));
            if (usr != null)
            {
                try
                {
                    var group = usr.Groups.FirstOrDefault(t => t.GroupNavigation.Name.Equals(name));
                    if (group == null) return "You do not have permission to add user to this group!";
                    var newUser = _context.Users.FirstOrDefault(t => t.NormalizedEmail.Equals(email.Normalize()));
                    if (newUser == null) return "User with this email does not exist!";
                    group.GroupNavigation.Users.Add(new UserGroup(newUser, GetGroup(group.GroupNavigation.Name)));
                    _context.SaveChanges();
                }
                catch (Exception)
                {
                    return "User is already a part of this group!";
                }
                    
            }
            return "User added!";
        }

        public Groups GetGroup(string groupName)
        {
            return _context.Groups.Include(t=>t.Records).FirstOrDefault(t => t.Name.Equals(groupName.Trim()));
        }

        public bool AddGroupRecord(Record record, Groups group)
        {
            try
            {
                group.Records.Add(record);
                _context.Records.Add(record);
                _context.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public Record EditRecordGroup(Guid id, string name, int typeNumber, decimal amount, string description, Groups group)
        {
            RemoveRecord(id);
            Record record = new Record(typeNumber, name, description, amount);
            AddGroupRecord(record, group);
            return record;
        }
    }
}
