using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Budgetco.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public DbSet<Record> Records { get; set; }
        public DbSet<Groups> Groups { get; set; }
        public DbSet<UserGroup> UserGroups { get; set; }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
            builder.Entity<Record>().ToTable("Record");
            builder.Entity<Groups>().ToTable("Group");
            builder.Entity<UserGroup>().ToTable("UserGroup");
            builder.Entity<IdentityUser>().ToTable("Users").Property(p => p.Id).HasColumnName("UserId");
            builder.Entity<ApplicationUser>().ToTable("MyUsers").Property(p => p.Id).HasColumnName("UserId");
            builder.Entity<ApplicationUser>().Property(t => t.FirstName);
            builder.Entity<ApplicationUser>().Property(t => t.LastName);
            builder.Entity<ApplicationUser>().HasMany(t => t.Records).WithOne();
            builder.Entity<UserGroup>().HasOne(k => k.GroupNavigation)
                .WithMany(k => k.Users)
                .HasForeignKey(k => k.GroupId);
            builder.Entity<UserGroup>().HasOne(k => k.UserNavigation)
                .WithMany(k => k.Groups)
                .HasForeignKey(k => k.UserId);
            builder.Entity<UserGroup>().HasKey(k => new { k.GroupId, k.UserId });
        }
    }
}
