﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Budgetco.Constants;

namespace Budgetco.Data.Models
{
    public class InputModel
    {
        [Required(ErrorMessage = "Name should not be empty!")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Please choose expense type")]
        public ExpenseType ExpenseType { get; set; }
        [Required(ErrorMessage = "Please choose income type")]
        public IncomeType IncomeType { get; set; }
        public string Description { get; set; }
        [Required(ErrorMessage = "Amount cannot be empty!")]
        public decimal Amount { get; set; }
        public List<Record> RecordModel { get; set; }
        public Dictionary<Record, UserGroup> PartialModel { get; set; }

        public InputModel()
        {
            RecordModel = new List<Record>();
            PartialModel = new Dictionary<Record, UserGroup>();
        }

        public decimal GetIncomeSum()
        {
            decimal sum = 0;
            foreach (var income in RecordModel)
            {
                if (income.Amount > decimal.Zero)
                {
                    sum += income.Amount;
                }
            }
            return sum;
        }
        public decimal GetExpenseSum()
        {
            decimal sum = 0;
            foreach (var expense in RecordModel)
            {
                if (expense.Amount < decimal.Zero)
                {
                    sum += expense.Amount;
                }
            }
            return sum;
        }
    }
}
