﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Budgetco.Data.Models
{
    public class GroupsViewModel
    {
        [DisplayName("Group name")]
        [Required(ErrorMessage = "You have to name your group!")]
        public string GroupName { get; set; }
        [EmailAddress(ErrorMessage = "The field is not a valid e-mail address")]
        public string InputEmail { get; set; }
        public List<UserGroup> Groups { get; set; }
        public InputModel InputRecord { get; set; }
        public Dictionary<Record, UserGroup> PartialModel { get; set; }

        public GroupsViewModel()
        {
            Groups = new List<UserGroup>();
            PartialModel = new Dictionary<Record, UserGroup>();
        }
    }
}
