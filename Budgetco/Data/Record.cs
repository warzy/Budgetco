﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Budgetco.Data
{
    public class Record
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        public DateTime Date { get; set; }
        [Required]
        public int Type { get; set; }
        [Required(ErrorMessage = "This is field is mandatory.")]
        public string Name { get; set; }
        [Required(ErrorMessage = "This is field is mandatory.")]
        public decimal Amount { get; set; }
        public string Description { get; set; }

        public Record()
        {

        }

        public Record(int typeNumber, string name, string description, decimal amount)
        {
            Id = Guid.NewGuid();
            Date = DateTime.UtcNow;
            Type = typeNumber;
            Name = name;
            Description = description;
            Amount = amount;
        }
    }
}
