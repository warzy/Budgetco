﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Budgetco.Data
{
    public class UserGroup
    {
        [Key]
        public Guid Id { get; set; }
        public string UserId { get; set; }
        public Guid GroupId { get; set; }
        public ApplicationUser UserNavigation { get; set; }
        public Groups GroupNavigation { get; set; }

        public UserGroup()
        {

        }
        public UserGroup(ApplicationUser user, Groups group)
        {
            Id = Guid.NewGuid();
            UserId = user.Id;
            GroupId = group.Id;
            UserNavigation = user;
            GroupNavigation = group;
        }
    }
}
