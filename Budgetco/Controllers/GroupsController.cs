﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using Budgetco.Constants;
using Budgetco.Data;
using Budgetco.Data.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Query.Internal;
using Newtonsoft.Json;

namespace Budgetco.Controllers
{
    public class GroupsController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IBudgetRepository _repository;

        public GroupsController(UserManager<ApplicationUser> userManager, IBudgetRepository repository)
        {
            _userManager = userManager;
            _repository = repository;
        }

        public async Task<IActionResult> Index()
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            var userGroups = _repository.GetGroups(user);
            var model = new GroupsViewModel {Groups = userGroups.OrderBy(t=>t.GroupNavigation.Name).ToList()};
            foreach (var group in userGroups)
            {
                foreach (var record in group.GroupNavigation.Records)
                {
                    model.PartialModel.Add(record, group);
                }
                
            }
            return View(model);
        }

        [HttpGet]
        public async Task<string> GetGroups()
        {
            ApplicationUser user = await _userManager.GetUserAsync(HttpContext.User);
            List<UserGroup> userGroups = _repository.GetGroups(user);
            List<string> groupNames = new List<string>();
            foreach (var group in userGroups)
            {
                groupNames.Add(group.GroupNavigation.Name);
            }

            return groupNames.Count == 0 ? null : JsonConvert.SerializeObject(groupNames);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<string> AddGroup(string name, string[] emails)
        {
            ApplicationUser user = await _userManager.GetUserAsync(HttpContext.User);
            List<string> filteredEmails = emails.ToList();
            //remove users email from list (he is automatically added to group as a creator)
            filteredEmails.RemoveAll(t => t.Equals(user.Email));
            //convert to set that removes duplicates and transform to list
            return _repository.AddGroup(name, filteredEmails.ToImmutableHashSet().ToList(), user);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<string> AddUserToGroup(string name, string email)
        {
            ApplicationUser user = await _userManager.GetUserAsync(HttpContext.User);
            if (user.NormalizedEmail.Equals(email.Normalize()))
                return "You cannot add yourself to a group you are already a part of.";

            return _repository.AddUserToGroup(name, email, user);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public Record AddRecord(string name, string type, decimal amount, string description, string groupName)
        {
            var group = _repository.GetGroup(groupName);
            if (Constants.Constants.ConstantsDictionary.ContainsKey(type) && group != null)
            {
                Constants.Constants.ConstantsDictionary.TryGetValue(type, out var typeNumber);
                Record record = new Record(typeNumber, name, description, amount);
                
                if (typeNumber < 3)
                {
                    record.Amount = Math.Abs(record.Amount);
                }
                else
                {
                    record.Amount = Math.Abs(record.Amount) * -1.0m;
                }

                return !_repository.AddGroupRecord(record, group) ? null : record;
            }
            return null;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<bool> RemoveRecord(string id)
        {
            Guid key = new Guid(id);
            var item = _repository.GetRecord(key);
            var user = await _userManager.GetUserAsync(HttpContext.User);
            var groups = _repository.GetGroups(user);
            foreach (var group in groups)
            {
                if (group.GroupNavigation.Records.Contains(item))
                {
                    _repository.RemoveRecord(key);
                    return true;
                }
            }
            return false;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public Record EditRecord(string id, string name, string type, decimal amount, string description, string groupName)
        {
            var group = _repository.GetGroup(groupName);
            if (Constants.Constants.ConstantsDictionary.ContainsKey(type))
            {
                Constants.Constants.ConstantsDictionary.TryGetValue(type, out var typeNumber);
                if (typeNumber < 3)
                {
                    return _repository.EditRecordGroup(new Guid(id), name, typeNumber, Math.Abs(amount), description, group);
                }
                return _repository.EditRecordGroup(new Guid(id), name, typeNumber, Math.Abs(amount) * -1, description, group);
            }
            return null;
        }

        [HttpGet]
        public async Task<string> GetConfig(DateTime dateFrom, DateTime dateTo, string transactionType,
                                            string transactionSubtype, string groupName, string xAxis)
        {
            List<Record> records = new List<Record>();
            if (groupName != null)
            {
                if (groupName.Equals("All Groups"))
                {
                    var user = await _userManager.GetUserAsync(HttpContext.User);
                    var groups = _repository.GetGroups(user);
                    foreach (var group in groups)
                    {
                        records.AddRange(group.GroupNavigation.Records);
                    }
                }
                else
                {
                    var group = _repository.GetGroup(groupName);
                    records = group.Records;
                }
            }
            else
            {
                var user = await _userManager.GetUserAsync(HttpContext.User);
                records = _repository.GetRecords(user);
            }

            if (dateFrom != DateTime.MinValue && dateTo != DateTime.MinValue)
            {
                records = records.Where(t => t.Date.Date >= dateFrom.Date && t.Date.Date <= dateTo.Date).ToList();
            }

            if (transactionType.Equals("expenses"))
            {
                records = records.Where(t => t.Amount < decimal.Zero).ToList();
                if (transactionSubtype != null)
                {
                    if (!transactionSubtype.Equals("allTypes"))
                    {
                        Constants.Constants.ConstantsDictionary.TryGetValue(transactionSubtype, out var typeNumber);
                        records = records.Where(t => t.Type == typeNumber).ToList();
                    }

                }
            }
            else if (transactionType.Equals("incomes"))
            {
                records = records.Where(t => t.Amount >= decimal.Zero).ToList();
                if (transactionSubtype != null)
                {
                    if (!transactionSubtype.Equals("allTypes"))
                    {
                        Constants.Constants.ConstantsDictionary.TryGetValue(transactionSubtype, out var typeNumber);
                        records = records.Where(t => t.Type == typeNumber).ToList();
                    }
                }
            }

            //If there is no records matching the conditions, return nothing
            if (records.Count == 0) return "";

            List<decimal> amounts = new List<decimal>();
            string[] labels = GetLabels(dateFrom, dateTo, amounts, records, xAxis);


            return JsonConvert.SerializeObject(new { amounts, labels });
        }

        private string[] GetLabels(DateTime dateFrom, DateTime dateTo, List<decimal> amounts, List<Record> records, string xAxis)
        {
            if (xAxis.Equals("Category"))
            {
                HashSet<string> categories = new HashSet<string>();
                Dictionary<int, decimal> sums = new Dictionary<int, decimal>();
                foreach (var record in records)
                {
                    categories.Add(record.Type < 3
                        ? ((IncomeType) record.Type).ToString()
                        : ((ExpenseType) record.Type).ToString());
                    if (sums.ContainsKey(record.Type))
                    {
                        sums.TryGetValue(record.Type, out var sum);
                        sums.Remove(record.Type);
                        sums.Add(record.Type, sum + record.Amount);
                    }
                    else
                    {
                        sums.Add(record.Type, record.Amount);
                    }
                }

                foreach (var sum in sums.Values)
                {
                    amounts.Add(sum);
                }
                return categories.ToArray();
            }
            if (dateTo == DateTime.MinValue && dateFrom == DateTime.MinValue)
            {
                dateTo = DateTime.MinValue;
                dateFrom = DateTime.MaxValue;
                foreach (var record in records)
                {
                    if (record.Date < dateFrom)
                    {
                        dateFrom = record.Date;
                    }

                    if (record.Date > dateTo)
                    {
                        dateTo = record.Date;
                    }
                }
            }
            if (dateTo > dateFrom.AddYears(1))
            {
                //years
                List<string> years = new List<string>();
                for (int i = dateFrom.Year; i <= dateTo.Year; i++)
                {
                    decimal sum = 0.0m;
                    years.Add(i + ".");
                    foreach (var record in records)
                    {
                        if (record.Date.Year.Equals(i))
                        {
                            sum += record.Amount;
                        }
                    }
                    amounts.Add(sum);
                }

                return years.ToArray();
            }
            else if (dateTo > dateFrom.AddMonths(1))
            {
                //months
                List<string> months = new List<string>();
                for (int i = dateFrom.Month; i <= dateTo.Month; i++)
                {
                    decimal sum = 0.0m;
                    DateTime dt = new DateTime(dateFrom.Ticks);
                    dt = dt.AddDays(i - dateFrom.DayOfYear);
                    months.Add(dt.Date.ToLongDateString() + ".");
                    foreach (var record in records)
                    {
                        if (record.Date.Month.Equals(i))
                        {
                            sum += record.Amount;
                        }
                    }
                    amounts.Add(sum);
                }

                return months.ToArray();
            }
            else
            {
                //days
                List<string> days = new List<string>();
                for (int i = dateFrom.DayOfYear; i <= dateTo.DayOfYear; i++)
                {
                    decimal sum = 0.0m;
                    DateTime dt = new DateTime(dateFrom.Ticks);
                    dt = dt.AddDays(i - dateFrom.DayOfYear);
                    days.Add(dt.DayOfWeek + " " + dt.Date.ToString("dd.MM.yyyy").Substring(0, 6));
                    foreach (var record in records)
                    {
                        if (record.Date.DayOfYear.Equals(i))
                        {
                            sum += record.Amount;
                        }
                    }
                    amounts.Add(sum);
                }

                return days.ToArray();
            }
        }
    }
}
