﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using Budgetco.Constants;
using Budgetco.Data;
using Newtonsoft.Json;
using Budgetco.Data.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Budgetco.Controllers
{
    [Authorize]
    public class MyRecordsController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IBudgetRepository _repository;

        public MyRecordsController(UserManager<ApplicationUser> userManager, IBudgetRepository repository)
        {
            _userManager = userManager;
            _repository = repository;
        }

        public async Task<IActionResult> Index()
        {
            ApplicationUser user = await _userManager.GetUserAsync(HttpContext.User);
            
            var inputModel = new InputModel
            {
                RecordModel = _repository.GetRecords(user)
            };
            foreach (var record in inputModel.RecordModel)
            {
                inputModel.PartialModel.Add(record, null);
            }
            return View(inputModel);
        }

        public IActionResult About()
        {
            return View();
        }
        public IActionResult Contact()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<Record> AddRecord(string name, string type, decimal amount, string description)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            if (Constants.Constants.ConstantsDictionary.ContainsKey(type) && user != null)
            {
                Constants.Constants.ConstantsDictionary.TryGetValue(type, out var typeNumber);
                Record record = new Record(typeNumber, name, description, amount);
                if (typeNumber < 3)
                {
                    record.Amount = Math.Abs(record.Amount);
                }
                else
                {
                    record.Amount = Math.Abs(record.Amount)*-1.0m;
                }

                if (!_repository.AddRecord(record, user))
                {
                    return null;
                }
                return record;
            }
            return null;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<bool> RemoveRecord(string id)
        {
            Guid key = new Guid(id);
            var user = await _userManager.GetUserAsync(HttpContext.User);
            user.Records = _repository.GetRecords(user);
            if (user.Records.Exists(t => t.Id.Equals(key)))
            {
                _repository.RemoveRecord(key);
                return true;
            }
            else return false;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public Record EditRecord(string id, string name, string type, decimal amount, string description)
        {
            if (Constants.Constants.ConstantsDictionary.ContainsKey(type))
            {
                Constants.Constants.ConstantsDictionary.TryGetValue(type, out var typeNumber);
                if (typeNumber < 3)
                {
                    return _repository.EditRecord(new Guid(id), name, typeNumber, Math.Abs(amount), description);
                }
                return _repository.EditRecord(new Guid(id), name, typeNumber, Math.Abs(amount)*-1, description);
            }
            return null;
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<int> HasDatabaseSetChanged(int records, bool groupView)
        {
            ApplicationUser user = await _userManager.GetUserAsync(HttpContext.User);
            if (groupView)
            {
                var count = 0;
                var groups = _repository.GetGroups(user);
                foreach (var group in groups)
                {
                    foreach (var record in group.GroupNavigation.Records)
                    {
                        count++;
                    }
                }

                return count;
            }
            else
            {
                return _repository.GetRecordsCount(user);
            }
        }

    }
}
