﻿function hasDatabaseSetChanged(recordsCount, groupView) {
    $.post("/MyRecords/HasDatabaseSetChanged",
        AddAntiForgeryToken({ "records": recordsCount, "groupView": groupView }),
        function (data) {
            if (data) {
                if (data !== parseInt(recordsCount)) {
                    updateCharts();
                    setTimeout(function () {
                        hasDatabaseSetChanged(data, groupView);
                        },
                        3000);
                }
                else {
                    setTimeout(function() {
                            hasDatabaseSetChanged(recordsCount, groupView);
                        },
                        3000);
                }
            }
        });
}

function resetDialog() {
    $("#expense-select").hide();
    $("#income-select").show(); 
}

function resetEditDialog() {
    $("#edit-expense-select").hide();
    $("#edit-income-select").hide();
}

AddAntiForgeryToken = function (data) {
    data.__RequestVerificationToken = $('#__AjaxAntiForgeryForm input[name=__RequestVerificationToken]').val();
    return data;
};

function renderEditDialog(tableRow, edit, editForm, url) {
    var oldName, oldAmount, oldDescription, oldType, id, recordType, oldGroup;
    $("#edit-form-div").dialog({
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
            resetEditDialog();
            var select = $("#edit-record-type-select").selectmenu({
                change: function (event, ui) {
                    if ($(this).find(":selected").text().valueOf() === "Income".valueOf()) {
                        recordType = "income-record";
                        $("#edit-expense-select").hide();
                        $("#edit-income-select").show();
                    } else {
                        recordType = "expense-record";
                        $("#edit-income-select").hide();
                        $("#edit-expense-select").show();
                    }
                }
            });
            
            id = tableRow.attr("value").valueOf();
            //get data from table (old data) and populate edit dialog
            var i = 0;
            tableRow.find("td").each(function () {
                switch (i) {
                    case 0:
                    {
                            oldName = $(this).text();
                            $("#edit-name").val(oldName);
                            break;
                        }
                    case 1:
                        {
                            if (tableRow.attr("class").valueOf() === "income-record") {
                                recordType = "income-record";
                                select.val("income").change();
                                $("#edit-income-select").show();
                                $("#edit-IncomeType").val($(this).text()).show();
                                oldType = $(this).text();

                            }
                            else {
                                select.val("expense").change();
                                recordType = "expense-record";
                                $("#edit-expense-select").show();
                                $("#edit-ExpenseType").val($(this).text()).show();
                                oldType = $(this).text();
                            }
                            $("#edit-record-type-select").selectmenu("refresh");

                            break;
                        }
                    case 2:
                    {
                            oldDescription = $(this).text();
                            $("#edit-item-description").val(oldDescription);
                            break;
                        }
                    case 3:
                    {
                            oldAmount = $(this).text();
                            $("#edit-amount").val(oldAmount);
                            break;
                        }
                }
                i++;
            });
            if (i === 7) {
                $("#group-name-edit-select").val(tableRow.find("td:eq(5)").text()).selectmenu("refresh");
            }
        },
        draggable: false,
        resizable: false,
        width: 350,
        modal: true,
        position: {
            my: "center top",
            at: "center bottom",
            of: edit
        },
        show: 300,
        hide: 300,
        autoOpen: false,
        buttons: {
            //ajax call to submit data
            "Submit": function () {
                editForm.validate();
                if (editForm.valid()) {
                    $(this).dialog("close");
                    //get data from input form (new data)
                    var name = $("#edit-name").val();
                    var amount = $("#edit-amount").val();
                    var description = $("#edit-item-description").val();
                    var type;
                    if (recordType === "income-record") {
                        type = $("#edit-IncomeType").val();
                    } else {
                        type = $("#edit-ExpenseType").val();
                    }
                    var group = $("#group-name-edit-select").val();
                    $.post(url,
                            AddAntiForgeryToken({ id: id, name: name, type: type, amount: amount, description: description, groupName: group}),
                            function (data) {
                                if (data != null) {
                                    //SUCCESS
                                    //Update data in the table
                                    var tr = $("tr[value=" + id + "]");
                                    var i = 0;
                                    if (isIncome(type)) {
                                        tr.removeClass().addClass("income-record");
                                        amount = Math.abs(amount);
                                    } else {
                                        tr.removeClass().addClass("expense-record");
                                        amount = Math.abs(amount) * -1;
                                    }
                                    tr.find("td").each(function() {
                                        switch (i) {
                                        case 0:
                                        {
                                            $(this).text(name);
                                            break;
                                        }
                                        case 1:
                                        {
                                            $(this).text(type);
                                            break;
                                        }
                                        case 2:
                                        {
                                            $(this).text(description);
                                            break;
                                        }
                                        case 3:
                                        {
                                            $(this).text(parseFloat(amount).toFixed(2));
                                            break;
                                        }
                                        case 4:
                                        {
                                            
                                            $(this).text(formatDate(data.date));
                                            break;
                                        }
                                        }
                                        i++;
                                    });
                                    if (i === 7) {
                                        tableRow.find("td:eq(5)").text(group);
                                    }
                                    $("#snackbar").text("Item updated successfully!");
                                    showSnackbar();

                                } else {
                                    //FAILURE
                                    $("#snackbar").text("There has been a problem updating your item. Try again...");
                                    showSnackbar();
                                }
                            });
                //If item is added or not -> reset edit select forms
                resetEditDialog();
                }
            },
            "Cancel": function () {
                $(this).dialog("close");
                $("#edit-input-form").trigger("reset");
                resetEditDialog();
            }
        }
    });
}
//Little alert window in the bottom of the screen
function showSnackbar() {
    // Get the snackbar DIV
    var x = document.getElementById("snackbar");

    // Add the "show" class to DIV
    x.className = "show";

    // After 3 seconds, remove the show class from DIV
    setTimeout(function () { x.className = x.className.replace("show", ""); }, 1500);
}

function formatDate(date) {
    var d = new Date(date);
    var day = d.getUTCDate();
    var month = d.getUTCMonth();
    month++;
    var year = d.getUTCFullYear();
    return (day < 10 ? "0" + day : day)
        + "." + (month < 10 ? "0" + month : month) + "." + year;
}

function isIncome(record) {
    var incomes = ["Salary", "Allowance", "Gift"];
    return incomes.includes(record) || record < 3;
}

function recordTypeFilter() {
    $.fn.dataTable.ext.search.push(
        function (settings, data, dataIndex) {
            if ($("#transactionTypesMenu").find(":selected").text().valueOf() === "Incomes".valueOf()) {
                if (isIncome(data[1])) {
                    return true;
                }
                return false;
            }
            else if ($("#transactionTypesMenu").find(":selected").text().valueOf() === "Expenses".valueOf()) {
                if (!isIncome(data[1])) {
                    return true;
                }
                return false;
            }
            else return true;
        }
    );
}

function addUserToGroup(email, groupName) {
    $.post("Groups/AddUserToGroup/",
        AddAntiForgeryToken({ name: groupName, email: email }),
        function(data) {
            $("#snackbar").text(data);
            showSnackbar();
        });
}

function updateGroups(table) {
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "/Groups/GetGroups",
        success: function (response) {
            if (response) {
                var options = [];
                response.forEach(function(item) {
                    options.push("<option value='" + item + "'>" + item + "</option>");
                });
                $("#addNewRecord").show();
                $("#group-name-select").empty().append(options.join("")).selectmenu();
                $("#group-name-edit-select").empty().append(options.join("")).selectmenu();
                $("#group-select-user").empty().append(options.join("")).selectmenu();
                options = [];
                options.push("<option value='" + "allRecords" + "'>" + "All Groups" + "</option>");
                response.forEach(function(item) {
                    options.push("<option value='" + item + "'>" + item + "</option>");
                });

                $("#group-select-chart").empty().append(options.join("")).selectmenu();
                $("#group-select").empty().append(options.join(""))
                    .selectmenu({
                        change: function(event, data) {
                            var val = $.fn.dataTable.util.escapeRegex($(this).val());
                            var column = table.column(5);
                            column.search(val !== "allRecords" ? '^' + val + '$' : '', true, false).draw();
                        }
                    });
                $("#group-select").selectmenu("refresh");
                $("#group-name-select").selectmenu("refresh");
                $("#group-select-chart").selectmenu("refresh");
                $("#group-name-edit-select").selectmenu("refresh");
                $("#group-select-user").selectmenu("refresh");
                $("#addNewUser").button().show();
            } else {
                $("#addNewUser").button().hide();
            }
        }
    });
}

function getEmailsAndResetForm(userCount) {
    var emails = new Array();
    for (var i = 0; i <= userCount; i++) {
        var inputField = $("#" + i);
        var text = inputField.val();
        if (text != null) {
            emails.push(inputField.val().trim());
        }
        if (i !== 0) {
            inputField.parent().remove();   
        }
    }
    return emails;
}

function dateFilter() {
    $.fn.dataTableExt.afnFiltering.push(
        function (oSettings, aData, iDataIndex) {
            var iFini = document.getElementById('datepicker_from').value;
            var iFfin = document.getElementById('datepicker_to').value;
            var iStartDateCol = 4;

            iFini = iFini.substring(6, 10) + iFini.substring(3, 5) + iFini.substring(0, 2);
            iFfin = iFfin.substring(6, 10) + iFfin.substring(3, 5) + iFfin.substring(0, 2);

            var datofini = aData[iStartDateCol].substring(6, 10) + aData[iStartDateCol].substring(3, 5) + aData[iStartDateCol].substring(0, 2);

            if (iFini === "" && iFfin === "") {
                return true;
            }
            else if (iFini <= datofini && iFfin === "") {
                return true;
            }
            else if (iFfin >= datofini && iFini === "") {
                return true;
            }
            else if (iFini <= datofini && iFfin >= datofini) {
                return true;
            }
            return false;
        }
    );
}

var currentNumberOfCharts;
function getChart(dateFrom, dateTo, chartType, transactionType, transactionSubtype, groupName, xAxis) {
    var dateFromString = dateFrom.substring(6, 10) + dateFrom.substring(3, 5) + dateFrom.substring(0, 2);
    var dateToString = dateTo.substring(6, 10) + dateTo.substring(3, 5) + dateTo.substring(0, 2);
    if ( (dateToString === "" && dateFromString !== "") || (dateToString !== "" && dateFromString === "")) {
        window.alert("You have to select date range or leave both fields empty!");
        return "";
    }
    else {
        if (dateToString < dateFromString) {
            window.alert("Selected date range is empty!");
            return "";
        }
        if (dateFromString === "" && dateToString === "") {
            dateFrom = null;
            dateTo = null;
        }
    }
    if (transactionType === "all") {
        transactionSubtype = null;
    }
    var dateFromDateString = null, dateToDateString = null;
    if (dateFrom !== null && dateTo !== null) {
        dateFromDateString =
            new Date(dateFrom.substring(6, 10) +
                "-" + dateFrom.substring(3, 5) +
                "-" + dateFrom.substring(0, 2)).toISOString();
        dateToDateString = new Date(dateTo.substring(6, 10) +
            "-" + dateTo.substring(3, 5) +
            "-" + dateTo.substring(0, 2)).toISOString();
    }
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "/Groups/GetConfig",
        data: {
            dateFrom: dateFromDateString,
            dateTo: dateToDateString,
            transactionType: transactionType, transactionSubtype: transactionSubtype, groupName: groupName, xAxis: xAxis
        },
        success: function (response) {
            if (response !== "") {
                var options = getChartOptions(chartType);
                var datasets = getDatasets(response, chartType);
                var config = {
                    type: chartType,
                    data: {
                        labels: response.labels,
                        datasets: datasets
                    },
                    options: options
                };
                var graphString = getChartString(currentNumberOfCharts);
                renderChart(graphString, currentNumberOfCharts, config);
                if (typeof (Storage) !== "undefined") {
                    if (groupName === null) {
                        localStorage.setItem("userChartNumber" + currentNumberOfCharts, currentNumberOfCharts);
                        localStorage.setItem("userOptions" + currentNumberOfCharts, JSON.stringify(options));
                        localStorage.setItem("userResponse" + currentNumberOfCharts, JSON.stringify(response));
                        localStorage.setItem("userChartType" + currentNumberOfCharts, chartType);
                        if (dateFrom !== null) {
                            localStorage.setItem("userDateFrom" + currentNumberOfCharts, dateFrom);
                        }
                        if (dateTo !== null) {
                            localStorage.setItem("userDateTo" + currentNumberOfCharts, dateTo);
                        }
                        if (transactionType !== null) {
                            localStorage.setItem("userTransactionType" + currentNumberOfCharts, transactionType);
                        }
                        if (transactionSubtype !== null) {
                            localStorage.setItem("userTransactionSubtype" + currentNumberOfCharts, transactionSubtype);
                        }
                        localStorage.setItem("userNumberOfCharts", currentNumberOfCharts);
                    } else {
                        localStorage.setItem("chartNumber" + currentNumberOfCharts, currentNumberOfCharts);
                        localStorage.setItem("options" + currentNumberOfCharts, JSON.stringify(options));
                        localStorage.setItem("response" + currentNumberOfCharts, JSON.stringify(response));
                        localStorage.setItem("chartType" + currentNumberOfCharts, chartType);
                        if (dateFrom !== null) {
                            localStorage.setItem("dateFrom" + currentNumberOfCharts, dateFrom);
                        }
                        if (dateTo !== null) {
                            localStorage.setItem("dateTo" + currentNumberOfCharts, dateTo);
                        }
                        if (transactionType !== null) {
                            localStorage.setItem("transactionType" + currentNumberOfCharts, transactionType);
                        }
                        if (transactionSubtype !== null) {
                            localStorage.setItem("transactionSubtype" + currentNumberOfCharts, transactionSubtype);
                        }
                        if (groupName !== null) {
                            localStorage.setItem("group" + currentNumberOfCharts, groupName);
                        }
                        localStorage.setItem("NumberOfCharts", currentNumberOfCharts);
                    }
                    
                }
                currentNumberOfCharts++;
            } else {
                alert("No records to show!");
            }
        }
    });
    return "";
}

function getChartString(chartNumber) {
    return '<div class="canvas chart"><br/><br/><canvas id="chart' +
        chartNumber +
        '"></canvas>' +
        '<button class="btn btn-sm btn-danger" value="' +
        chartNumber +
        '" id="removeGraphButton' +
        chartNumber +
        '" onclick="removeChartDiv(this.id, this.value)">Remove chart</button>' +
        '&nbsp;<button data-toggle="collapse" data-target="#edit-chart' + chartNumber +
        '" id="edit-chart-button' +chartNumber +'">Edit chart</button>' +
        '<br/><br/>' + '<div id="edit-chart'+chartNumber+'" class="collapse">' +
        '<form id="edit-chart-form' + chartNumber +'">' +
        ' <label for="chart-type">Chart type:</label>' +
        ' <select id="chart-type' + chartNumber +'">' +
        ' <option value="line">Line</option>' +
        '<option value="bar">Bar</option>' +
        '<option value="radar">Radar</option>' +
        '<option value="doughnut">Doughnut</option>' +
        '<option value="pie">Pie</option>' +
        '<option value="polarArea">Polar area</option>' +
        '</select>' +
        '<br /><br />' +
        '<label for="transaction-type-chart' + chartNumber +'">Choose transactions to show:</label>' +
        '<select id="transaction-type-chart'+chartNumber+'">' +
        '<option value="all">All transactions</option>' +
        '<option value="expenses">Expenses</option>' +
        '<option value="incomes">Incomes</option>' +
        '</select>' +
        '<br /><br />' +
        '<div id="transaction-subtype-chart-div' + chartNumber +'">' +
        '<label>Transaction subtype:</label>' +
        '<select id="transaction-subtype-chart' + chartNumber +'"></select>' +
        '<br /><br />' +
        '</div>' +
        '<div>' +
        '<label for="x-axis-select' + chartNumber +'">X-axis type:</label>' +
        '<select id="x-axis-select' + chartNumber +'">' +
        '<option value="Time">Time</option>' +
        '<option value="Category">Category</option>' +
        '</select>' +
        '</div>' +
        '<div id="date-range-chart' + chartNumber +'" class="row">' +
        '<div class="col-lg-2">' +
        '<label id="date-label-from' + chartNumber + '">From: </label><input class="form-control" style="background: url(https://cdn2.iconfinder.com/data/icons/business-2/512/Icon_14-16.png) no-repeat; background-Position: 97% center; cursor: pointer;" type="text" id="datepicker_from_chart' + chartNumber +'" />' +
        '</div>' +
        '<div class="col-lg-2">' +
        '<label id="date-label-to' + chartNumber + '">To: </label><input class="form-control" style="background: url(https://cdn2.iconfinder.com/data/icons/business-2/512/Icon_14-16.png) no-repeat; background-Position: 97% center; cursor: pointer;" type="text" id="datepicker_to_chart' + chartNumber +'" />' +
        '</div>' +
        '</div>' +
        '</form>' +
        '<br /> <br />' +
        '<button id="submit-chart' + chartNumber +'">Submit</button>' +
        '<button type="reset" value="reset" class="btn btn-sm btn-danger" id="reset-button' + chartNumber +'">Reset</button>' +
        '<br/><br/></div ></div>';
}

function removeChartDiv(id, numberOfCharts) {
    if ($("#pageTitle").text() === "My records") {
        localStorage.removeItem("userChartNumber" + numberOfCharts);
        localStorage.removeItem("userOptions" + numberOfCharts);
        localStorage.removeItem("userResponse" + numberOfCharts);
        localStorage.removeItem("userChartType" + numberOfCharts);
        localStorage.removeItem("userDateFrom" + numberOfCharts);
        localStorage.removeItem("userDateTo" + numberOfCharts);
        localStorage.removeItem("userTransactionType" + numberOfCharts);
        localStorage.removeItem("userTransactionSubtype" + numberOfCharts);
    }
    else {
        localStorage.removeItem("chartNumber" + numberOfCharts);
        localStorage.removeItem("options" + numberOfCharts);
        localStorage.removeItem("response" + numberOfCharts);
        localStorage.removeItem("chartType" + numberOfCharts);
        localStorage.removeItem("dateFrom" + numberOfCharts);
        localStorage.removeItem("dateTo" + numberOfCharts);
        localStorage.removeItem("transactionType" + numberOfCharts);
        localStorage.removeItem("transactionSubtype" + numberOfCharts);
        localStorage.removeItem("group" + numberOfCharts);
    }
    $("#" + id).closest("div").remove();
}

function indexCharts() {
    var foundCharts = 1;
    for (var i = 1; i <= localStorage.getItem("NumberOfCharts"); i++) {
        var item = localStorage.getItem("chartNumber" + i);
        if (item !== null) {
            localStorage.removeItem("chartNumber" + i);
            localStorage.setItem("chartNumber" + foundCharts, foundCharts);
            item = localStorage.getItem("options" + i);
            localStorage.removeItem("options" + i);
            localStorage.setItem("options" + foundCharts, item);
            item = localStorage.getItem("response" + i);
            localStorage.removeItem("response" + i);
            localStorage.setItem("response" + foundCharts, item);
            item = localStorage.getItem("chartType" + i);
            localStorage.removeItem("chartType" + i);
            localStorage.setItem("chartType" + foundCharts, item);
            item = localStorage.getItem("dateFrom" + i);
            if (item !== null) {
                localStorage.removeItem("dateFrom" + i);
                localStorage.setItem("dateFrom" + foundCharts, item);
            }
            item = localStorage.getItem("dateTo" + i);
            if (item !== null) {
                localStorage.removeItem("dateTo" + i);
                localStorage.setItem("dateTo" + foundCharts, item);
            }
            
            item = localStorage.getItem("transactionType" + i);
            if (item !== null) {
                localStorage.removeItem("transactionType" + i);
                localStorage.setItem("transactionType" + foundCharts, item);
            }
            item = localStorage.getItem("transactionSubtype" + i);
            if (item !== null) {
                localStorage.removeItem("transactionSubtype" + i);
                localStorage.setItem("transactionSubtype" + foundCharts, item);
            }
            item = localStorage.getItem("group" + i);
            if (item !== null) {
                localStorage.removeItem("group" + i);
                localStorage.setItem("group" + foundCharts, item);
            }
            foundCharts++;
        }
    }
    if (localStorage.getItem("NumberOfCharts") !== null) {
        localStorage.setItem("NumberOfCharts", --foundCharts);
    }
}

function indexUserCharts() {
    var foundCharts = 1;
    for (var i = 1; i <= localStorage.getItem("userNumberOfCharts"); i++) {
        var item = localStorage.getItem("userChartNumber" + i);
        if (item !== null) {
            localStorage.removeItem("userChartNumber" + i);
            localStorage.setItem("userChartNumber" + foundCharts, foundCharts);
            item = localStorage.getItem("userOptions" + i);
            localStorage.removeItem("userOptions" + i);
            localStorage.setItem("userOptions" + foundCharts, item);
            item = localStorage.getItem("userResponse" + i);
            localStorage.removeItem("userResponse" + i);
            localStorage.setItem("userResponse" + foundCharts, item);
            item = localStorage.getItem("userChartType" + i);
            localStorage.removeItem("userChartType" + i);
            localStorage.setItem("userChartType" + foundCharts, item);
            
            item = localStorage.getItem("userDateFrom" + i);
            if (item !== null) {
                localStorage.removeItem("userDateFrom" + i);
                localStorage.setItem("userDateFrom" + foundCharts, item);
            }
            
            item = localStorage.getItem("userDateTo" + i);
            if (item !== null) {
                localStorage.removeItem("userDateTo" + i);
                localStorage.setItem("userDateTo" + foundCharts, item);
            }
            
            
            item = localStorage.getItem("userTransactionType" + i);
            if (item !== null) {
                localStorage.removeItem("userTransactionType" + i);
                localStorage.setItem("userTransactionType" + foundCharts, item);
            }
            
            item = localStorage.getItem("userTransactionSubtype" + i);
            if (item !== null) {
                localStorage.removeItem("userTransactionSubtype" + i);
                localStorage.setItem("userTransactionSubtype" + foundCharts, item);
            }
            
            foundCharts++;
        }
    }
    if (localStorage.getItem("userNumberOfCharts") !== null) {
        localStorage.setItem("userNumberOfCharts", --foundCharts);
    }
}

function renderChart(chartString, i, config) {
    $(".canvas").last().after(chartString);   

    var expenses = [
        "Groceries", "Rent", "Car", "Bills", "Entertainment",
        "Electronics", "Clothes", "Toiletries", "Eating Out",
        "Gifts", "Health", "House", "Pets", "Alcohol", "Tobacco"
    ];
    var incomes = ["Salary", "Allowance", "Gift"];

    var subTypeMenuChart = $('#transaction-subtype-chart' + i).selectmenu();

    var transactionTypeChart = $('#transaction-type-chart' + i).selectmenu({
        change: function (event, ui) {
            var options = [];
            if ($(this).find(":selected").text().valueOf() === "Incomes".valueOf()) {
                options.push("<option value='" + "allTypes" + "'>" + "All Types" + "</option>");
                incomes.forEach(function (item) {
                    options.push("<option value='" + item
                        + "'>" + item + "</option>");
                });
                subTypeMenuChart.empty().append(options.join("")).selectmenu("widget");
                $('#transaction-subtype-chart-div' + i).show();
            } else if ($(this).find(":selected").text().valueOf() === "Expenses".valueOf()) {
                options.push("<option value='" + "allTypes" + "'>" + "All Types" + "</option>");
                expenses.forEach(function (item) {
                    options.push("<option value='" + item
                        + "'>" + item + "</option>");
                });
                subTypeMenuChart.empty().append(options.join("")).selectmenu("widget");
                $('#transaction-subtype-chart-div' + i).show();
            } else {
                subTypeMenuChart.val("allTypes");
                subTypeMenuChart.selectmenu("refresh");
                $('#transaction-subtype-chart-div' + i).hide();
            }

            subTypeMenuChart.selectmenu("refresh");
        }
    });


    $('#edit-chart-button' + i).button();
    $('#transaction-subtype-chart-div' + i).hide();

    var dateFromChart = $('#datepicker_from_chart' + i).datepicker({
        dateFormat: 'dd.mm.yy',
        showButtonPanel: true,
        changeMonth: true,
        changeYear: true,
        autoSize: true
    });

    var dateToChart = $('#datepicker_to_chart' + i).datepicker({
        dateFormat: 'dd.mm.yy',
        showButtonPanel: true,
        changeMonth: true,
        changeYear: true,
        autoSize: true
    });

    $('#reset-button' + i).button().click(function () {
        $('#edit-chart-form' + i)[0].reset();


        if (transactionTypeChart.find(":selected").text().valueOf() === "Incomes".valueOf()) {
            $('#transaction-subtype-chart-div' + i).show();
        } else if (transactionTypeChart.find(":selected").text().valueOf() === "Expenses".valueOf()) {
            $('#transaction-subtype-chart-div' + i).show();
        } else {
            $('#transaction-subtype-chart-div' + i).hide();
        }
    });

    var xAxis = $('#x-axis-select' + i).selectmenu();

    var chartType = $('#chart-type' + i).selectmenu();
    $('#submit-chart' + i).button().click(function () {
        removeChartDiv("chart" + i, i);
        getChart((dateFromChart.val()),
            (dateToChart.val()),
            chartType.val(),
            transactionTypeChart.val(),
            subTypeMenuChart.val(),
            null,
            xAxis.val()
        );
    });
    var ctx = document.getElementById("chart" + i).getContext("2d");
    var newChart = new Chart(ctx, config);
}

function getDatasets(response, chartType) {
    var colors = getColors(response);
    var dataset;
    if (chartType === "pie" || chartType === "doughnut" || chartType ==="polarArea") {
        dataset = [
            {
                label: "Data",
                data: response.amounts,
                borderColor: colors,
                backgroundColor: colors
            }
        ];
        return dataset;
    }
    if (chartType === "radar") {
        dataset = [
            {
                label: "Data",
                data: response.amounts,
                borderColor: colors[0],
                backgroundColor: colors[0]
            }
        ];
        return dataset;
    }
    //line or bar
    dataset = [
        {
            label: "Data",
            data: response.amounts,
            borderColor: colors[0],
            backgroundColor: colors[0],
            fill: false
        }
    ];
    return dataset;
}

function getChartOptions(type) {
    var options = {
        title: {
            display: true,
            text: type + " chart!"
        }
    };
    return options;
}

function getColors(response) {
    var colors = [];
    response.labels.forEach(function () {
        colors.push("rgb(" +
            Math.floor(Math.random() * 255) +
            ", " +
            Math.floor(Math.random() * 255) +
            ", " +
            Math.floor(Math.random() * 255) +
            ")");
    });
    return colors;
}

function renderStorageGraphs() {
    var numberOfCharts;
    if ($("#pageTitle").text() === "My records") {
        numberOfCharts = localStorage.getItem("userNumberOfCharts");
    }
    else {
        numberOfCharts = localStorage.getItem("NumberOfCharts");
    }
    for (var i = 1; i <= numberOfCharts ; i++) {
        var chartString;
        var config;
        var response = JSON.parse(localStorage.getItem("response" + i));
        var userResponse = JSON.parse(localStorage.getItem("userResponse" + i));
        if (response !== null && response !== undefined && $("#pageTitle").text() !== "My records") {
            var chartNumber = localStorage.getItem("chartNumber" + i);
            var chartType = localStorage.getItem("chartType" + i);
            var options = JSON.parse(localStorage.getItem("options" + i));
            var datasets = getDatasets(response, chartType);
            chartString = getChartString(chartNumber);
            config = {
                type: chartType,
                data: {
                    labels: response.labels,
                    datasets: datasets
                },
                options: options
            };
        }
        else if (userResponse !== null && userResponse !== undefined && $("#pageTitle").text() === "My records") {
            var userChartNumber = localStorage.getItem("userChartNumber" + i);
            var userchartType = localStorage.getItem("userChartType" + i);
            var useroptions = JSON.parse(localStorage.getItem("userOptions" + i));
            var userdatasets = getDatasets(userResponse, userchartType);
            chartString = getChartString(userChartNumber);
            config = {
                type: userchartType,
                data: {
                    labels: userResponse.labels,
                    datasets: userdatasets
                },
                options: useroptions
            };
        } 
        else {
            continue;
        }
        
        renderChart(chartString, i, config);
    }
}

function updateCharts() {
    var numberOfCharts;
    if ($("#pageTitle").text() === "My records") {
        numberOfCharts = localStorage.getItem("userNumberOfCharts");
    }
    else {
        numberOfCharts = localStorage.getItem("NumberOfCharts");
    }
    for (var i = 1; i <= numberOfCharts; i++) {
        var response = JSON.parse(localStorage.getItem("response" + i));
        var userResponse = JSON.parse(localStorage.getItem("userResponse" + i));
        if (response !== null && response !== undefined && $("#pageTitle").text() !== "My records") {
            var dateFrom = localStorage.getItem("dateFrom" + i);
            alert(dateFrom);
            if (dateFrom === null || dateFrom === undefined) dateFrom = "";
            var dateTo = localStorage.getItem("dateTo" + i);
            if (dateTo === null || dateTo === undefined) dateTo = "";
            var chartType = localStorage.getItem("chartType" + i);
            var transactionType = localStorage.getItem("transactionType" + i);
            var transactionSubtype = localStorage.getItem("transactionSubtype" + i);
            var groupName = localStorage.getItem("group" + i);

            removeChartDiv("chart" + i, i);
            getChart(dateFrom, dateTo, chartType, transactionType, transactionSubtype, groupName);
        }
        else if (userResponse !== null && userResponse !== undefined && $("#pageTitle").text() === "My records") {
            var userDateFrom = localStorage.getItem("userDateFrom" + i);
            if (userDateFrom === null || userDateFrom === undefined) userDateFrom = "";
            var userDateTo = localStorage.getItem("userDateTo" + i);
            if (userDateTo === null || userDateTo === undefined) userDateTo = "";
            var userChartType = localStorage.getItem("userChartType" + i);
            var userTransactionType = localStorage.getItem("userTransactionType" + i);
            var userTransactionSubtype = localStorage.getItem("userTransactionSubtype" + i);

            removeChartDiv("chart" + i, i);
            getChart(userDateFrom, userDateTo, userChartType, userTransactionType, userTransactionSubtype, null);
        }
        else {
            continue;
        }
    }
}


function makeChartsDraggable() {
    $(".all-charts").sortable({
        placeholder: "ui-state-highlight1"
    });
    $(".all-charts").disableSelection();
}
