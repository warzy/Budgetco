﻿using System.Collections.Generic;

namespace Budgetco.Constants
{
    public enum ExpenseType
    {
        Groceries = 3, Rent, Car, Bills, Entertainment,
        Electronics, Clothes, Toiletries, EatingOut,
        Gifts, Health, House, Pets, Alcohol, Tobacco
    }
    public enum IncomeType { Salary, Allowance, Gift }

    public static class Constants
    {
        public static Dictionary<string, int> ConstantsDictionary = new Dictionary<string, int>()
        {
            {"Salary", 0},
            {"Allowance", 1},
            {"Gift", 2},
            {"Groceries", 3},
            {"Rent", 4},
            {"Car", 5},
            {"Bills", 6},
            {"Entertainment", 7},
            {"Electronics", 8},
            {"Clothes", 9},
            {"Toiletries", 10},
            {"Eating Out", 11},
            {"Gifts", 12},
            {"Health", 13},
            {"House", 14},
            {"Pets", 15},
            {"Alcohol", 16},
            {"Tobacco", 17}
        };

        public static ExpenseType GetExpenseType(int type)
        {
            switch (type)
            {
                case 3: return ExpenseType.Groceries;
                case 4: return ExpenseType.Rent;
                case 5: return ExpenseType.Car;
                case 6: return ExpenseType.Bills;
                case 7: return ExpenseType.Entertainment;
                case 8: return ExpenseType.Electronics;
                case 9: return ExpenseType.Clothes;
                case 10: return ExpenseType.Toiletries;
                case 11: return ExpenseType.EatingOut;
                case 12: return ExpenseType.Gifts;
                case 13: return ExpenseType.Health;
                case 14: return ExpenseType.House;
                case 15: return ExpenseType.Pets;
                case 16: return ExpenseType.Alcohol;
                case 17: return ExpenseType.Tobacco;
            }

            //This should never be reached
            return 0;
        }

        public static IncomeType GetIncomeType(int type)
        {
            switch (type)
            {
                case 0: return IncomeType.Salary;
                case 1: return IncomeType.Allowance;
                case 2: return IncomeType.Gift;
            }

            //This should never be reached
            return 0;
        }
    }
}